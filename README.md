# Voucher Service

Voucher Service is a simple RESTful API built using Spring Boot that manages vouchers. It provides endpoints for creating, retrieving, and managing vouchers.

## Table of Contents

- [Technologies](#technologies)
- [Installation](#installation)
- [API Endpoints](#api-endpoints)
- [License](#license)

## Technologies

- Java 17
- Spring Boot 3.0.5
- Spring Data JPA
- H2 Database
- Lombok
- JUnit 5
- Mockito

## Installation

1. Clone the repository:
   git clone git@gitlab.com:marius.manolache95/voucher-service.git
2. Navigate to the project directory:
   cd voucher-service
3. Build the project using Maven:
   mvn clean install
4. Run the application:
   mvn spring-boot:run

## API Endpoints

### Get All Vouchers

- **URL:** /api/v1/vouchers
- **Method:** GET
- **Response:** A list of all vouchers in the database.

### Get Voucher by ID

- **URL:** /api/v1/vouchers/{id}
- **Method:** GET
- **Response:** The voucher with the specified ID.

### Create Voucher

- **URL:** /api/v1/vouchers
- **Method:** POST
- **Request Body:** A JSON object containing the voucher details.
- **Response:** The created voucher.

### Invalidate Voucher

- **URL:** /api/v1/vouchers/{id}/invalidate
- **Method:** PUT
- **Response:**
  - 200 OK: The voucher with the specified ID has been successfully invalidated.
  - 400 Bad Request: The voucher with the specified ID is already invalidated.
  - 404 Not Found: The voucher with the specified ID does not exist in the database.
