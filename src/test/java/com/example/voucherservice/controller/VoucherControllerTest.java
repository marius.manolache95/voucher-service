package com.example.voucherservice.controller;

import com.example.voucherservice.model.DiscountType;
import com.example.voucherservice.model.Voucher;
import com.example.voucherservice.service.CurrencyService;
import com.example.voucherservice.service.VoucherService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class VoucherControllerTest {

    @Mock
    private VoucherService voucherService;

    @Mock
    private CurrencyService currencyService;

    @InjectMocks
    private VoucherController voucherController;

    private MockMvc mockMvc;

    @BeforeEach
    void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(voucherController).build();
    }

    @Test
    void invalidateVoucherScenarios() throws Exception {

        Long nonExistentVoucherId = 2L;
        Long invalidVoucherId = 3L;
        Long validVoucherId = 4L;

        // Scenario 1: Voucher not found (404)
        doThrow(NoSuchElementException.class)
                .when(voucherService)
                .invalidateVoucher(nonExistentVoucherId);

        mockMvc.perform(put("/api/v1/vouchers/{id}/invalidate", nonExistentVoucherId))
                .andExpect(status().isNotFound());

        // Scenario 2: Voucher already invalidated (400)
        doThrow(IllegalStateException.class)
                .when(voucherService)
                .invalidateVoucher(invalidVoucherId);

        mockMvc.perform(put("/api/v1/vouchers/{id}/invalidate", invalidVoucherId))
                .andExpect(status().isBadRequest());

        // Scenario 3: Voucher invalidated successfully (200)
        doNothing()
                .when(voucherService)
                .invalidateVoucher(validVoucherId);

        mockMvc.perform(put("/api/v1/vouchers/{id}/invalidate", validVoucherId))
                .andExpect(status().isOk());
    }

    @Test
    void getVoucherByIdWithDifferentCurrency() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(voucherController).build();

        Voucher initialVoucher = new Voucher();
        initialVoucher.setId(1L);
        initialVoucher.setCode("TEST123");
        initialVoucher.setDiscountType(DiscountType.PERCENTAGE);
        initialVoucher.setValue(new BigDecimal("10.00"));
        initialVoucher.setValid(true);
        initialVoucher.setUsageType(Voucher.UsageType.MULTIPLE_USE);
        initialVoucher.setCurrency("USD");

        when(voucherService.getVoucherById(1L, "EUR")).thenReturn(initialVoucher);

        Voucher expectedVoucher = new Voucher();
        expectedVoucher.setId(1L);
        expectedVoucher.setCode("TEST123");
        expectedVoucher.setDiscountType(DiscountType.PERCENTAGE);
        expectedVoucher.setValue(new BigDecimal("9.00"));
        expectedVoucher.setValid(true);
        expectedVoucher.setUsageType(Voucher.UsageType.MULTIPLE_USE);
        expectedVoucher.setCurrency("EUR");

        when(voucherService.getVoucherById(1L, "EUR")).thenReturn(expectedVoucher);

        mockMvc.perform(get("/api/v1/vouchers/1")
                        .param("currency", "EUR"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1L))
                .andExpect(jsonPath("$.code").value("TEST123"))
                .andExpect(jsonPath("$.discountType").value("PERCENTAGE"))
                .andExpect(jsonPath("$.value").value(9.0))
                .andExpect(jsonPath("$.valid").value(true))
                .andExpect(jsonPath("$.usageType").value("MULTIPLE_USE"))
                .andExpect(jsonPath("$.currency").value("EUR"));
    }

    @Test
    void getAllVouchersWithCurrencyConversion() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(voucherController).build();

        Voucher initialVoucher1 = new Voucher();
        initialVoucher1.setId(1L);
        initialVoucher1.setCode("TEST123");
        initialVoucher1.setDiscountType(DiscountType.PERCENTAGE);
        initialVoucher1.setValue(new BigDecimal("10.00"));
        initialVoucher1.setValid(true);
        initialVoucher1.setUsageType(Voucher.UsageType.MULTIPLE_USE);
        initialVoucher1.setCurrency("USD");

        Voucher initialVoucher2 = new Voucher();
        initialVoucher2.setId(2L);
        initialVoucher2.setCode("TEST456");
        initialVoucher2.setDiscountType(DiscountType.AMOUNT);
        initialVoucher2.setValue(new BigDecimal("5.00"));
        initialVoucher2.setValid(true);
        initialVoucher2.setUsageType(Voucher.UsageType.SINGLE_USE);
        initialVoucher2.setCurrency("USD");

        Voucher expectedVoucher1 = new Voucher();
        expectedVoucher1.setId(1L);
        expectedVoucher1.setCode("TEST123");
        expectedVoucher1.setDiscountType(DiscountType.PERCENTAGE);
        expectedVoucher1.setValue(new BigDecimal("9.00"));
        expectedVoucher1.setValid(true);
        expectedVoucher1.setUsageType(Voucher.UsageType.MULTIPLE_USE);
        expectedVoucher1.setCurrency("EUR");

        Voucher expectedVoucher2 = new Voucher();
        expectedVoucher2.setId(2L);
        expectedVoucher2.setCode("TEST456");
        expectedVoucher2.setDiscountType(DiscountType.AMOUNT);
        expectedVoucher2.setValue(new BigDecimal("4.50"));
        expectedVoucher2.setValid(true);
        expectedVoucher2.setUsageType(Voucher.UsageType.SINGLE_USE);
        expectedVoucher2.setCurrency("EUR");

        List<Voucher> expectedVouchers = new ArrayList<>();
        expectedVouchers.add(expectedVoucher1);
        expectedVouchers.add(expectedVoucher2);

        when(voucherService.getAllVouchers("EUR")).thenReturn(expectedVouchers);

        mockMvc.perform(get("/api/v1/vouchers")
                        .param("currency", "EUR"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id").value(1L))
                .andExpect(jsonPath("$[0].code").value("TEST123"))
                .andExpect(jsonPath("$[0].discountType").value("PERCENTAGE"))
                .andExpect(jsonPath("$[0].value").value(9.0))
                .andExpect(jsonPath("$[0].valid").value(true))
                .andExpect(jsonPath("$[0].usageType").value("MULTIPLE_USE"))
                .andExpect(jsonPath("$[0].currency").value("EUR"))
                .andExpect(jsonPath("$[1].id").value(2L))
                .andExpect(jsonPath("$[1].code").value("TEST456"))
                .andExpect(jsonPath("$[1].discountType").value("AMOUNT"))
                .andExpect(jsonPath("$[1].value").value(4.5))
                .andExpect(jsonPath("$[1].valid").value(true))
                .andExpect(jsonPath("$[1].usageType").value("SINGLE_USE"))
                .andExpect(jsonPath("$[1].currency").value("EUR"));
    }
}
