package com.example.voucherservice.service;

import com.example.voucherservice.model.DiscountType;
import com.example.voucherservice.model.Voucher;
import com.example.voucherservice.repository.VoucherRepository;
import com.example.voucherservice.util.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class VoucherServiceImplTest {

    @Mock
    private VoucherRepository voucherRepository;

    @Mock
    private CurrencyService currencyService;

    @InjectMocks
    private VoucherServiceImpl voucherService;

    private Voucher voucher;

    @BeforeEach
    void setUp() {
        voucher = new Voucher();
        voucher.setId(1L);
        voucher.setCode("TESTCODE");
        voucher.setDiscountType(DiscountType.PERCENTAGE);
        voucher.setValue(BigDecimal.valueOf(20.0));
        voucher.setValid(true);
        voucher.setUsageType(Voucher.UsageType.SINGLE_USE);
    }

    @Test
    void createVoucher() {
        when(voucherRepository.save(any(Voucher.class))).thenReturn(voucher);

        Voucher createdVoucher = voucherService.createVoucher(voucher);

        assertEquals(voucher, createdVoucher);
        verify(voucherRepository, times(1)).save(voucher);
    }

    @Test
    void getVoucherById() {
        when(voucherRepository.findById(1L)).thenReturn(Optional.of(voucher));

        Voucher foundVoucher = voucherService.getVoucherById(1L, Constants.DEFAULT_CURRENCY);

        assertEquals(voucher, foundVoucher);
        assertEquals(Constants.DEFAULT_CURRENCY, foundVoucher.getCurrency());
        verify(voucherRepository, times(1)).findById(1L);
    }

    @Test
    void invalidateVoucherSuccess() {
        when(voucherRepository.findById(1L)).thenReturn(Optional.of(voucher));
        when(voucherRepository.save(any(Voucher.class))).thenReturn(voucher);

        voucherService.invalidateVoucher(1L);

        assertFalse(voucher.getValid());
        verify(voucherRepository, times(1)).findById(1L);
        verify(voucherRepository, times(1)).save(voucher);
    }

    @Test
    void shouldNotInvalidateNonExistentVoucher() {
        when(voucherRepository.findById(1L)).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> voucherService.invalidateVoucher(1L));

        verify(voucherRepository, times(1)).findById(1L);
    }

    @Test
    void shouldNotInvalidateAlreadyInvalidVoucher() {
        voucher.setValid(false);
        when(voucherRepository.findById(1L)).thenReturn(Optional.of(voucher));

        assertThrows(IllegalStateException.class, () -> voucherService.invalidateVoucher(1L));

        verify(voucherRepository, times(1)).findById(1L);
    }

    @Test
    public void createVoucherWithLowerCaseCodeShouldSaveUpperCaseCode() {
        voucher.setCode("save1000");

        when(voucherRepository.save(any(Voucher.class))).thenReturn(voucher);

        Voucher savedVoucher = voucherService.createVoucher(voucher);

        assertEquals("SAVE1000", savedVoucher.getCode());
    }

    @Test
    public void createVoucherWithInvalidCodeShouldThrowException() {
        voucher.setCode("save$$33");

        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));

        voucher.setCode("SAVE!");

        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));

        voucher.setCode("SAV E10");

        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));

        voucher.setCode(" ");

        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));

        voucher.setCode("");

        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));
    }

    @Test
    public void createVoucherWithIncorrectStructureShouldThrowException() {
        voucher.setValue(null);

        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));

        setUp();
        voucher.setUsageType(null);
        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));

        setUp();
        voucher.setCode(null);
        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));

        setUp();
        voucher.setDiscountType(null);
        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));
    }

    @Test
    public void createVoucherWithDuplicateCodeShouldThrowException() {
        when(voucherRepository.findByCode(voucher.getCode())).thenReturn(Optional.empty());
        when(voucherRepository.save(any(Voucher.class))).thenReturn(voucher);

        Voucher firstVoucher = voucherService.createVoucher(voucher);
        assertEquals(voucher, firstVoucher);

        when(voucherRepository.findByCode(voucher.getCode())).thenReturn(Optional.of(firstVoucher));

        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher));
    }

    @Test
    void shouldGetAllVouchersWithCurrency() {
        // given
        List<Voucher> vouchersWithUsdCurrency = createVouchersWithCurrency("USD");
        when(voucherRepository.findAll()).thenReturn(vouchersWithUsdCurrency);
        when(currencyService.convertAmount(any(BigDecimal.class), any(String.class)))
                .thenAnswer(invocation -> ((BigDecimal) invocation.getArgument(0)).multiply(BigDecimal.valueOf(0.85)));

        List<Voucher> result = voucherService.getAllVouchers("EUR");

        assertNotNull(result);
        assertEquals(vouchersWithUsdCurrency.size(), result.size());
        assertAll(
                () -> assertEquals("EUR", result.get(0).getCurrency()),
                () -> assertEquals(BigDecimal.valueOf(85).setScale(2, RoundingMode.HALF_UP), result.get(0).getValue())
        );
    }

    @Test
    void shouldGetVoucherByIdWithCurrency() {
        // given
        Voucher voucher = createVoucherWithCurrency(1L, "USD");
        when(voucherRepository.findById(1L)).thenReturn(Optional.of(voucher));
        when(currencyService.convertAmount(any(BigDecimal.class), any(String.class)))
                .thenAnswer(invocation -> ((BigDecimal) invocation.getArgument(0)).multiply(BigDecimal.valueOf(0.85)));

        Voucher result = voucherService.getVoucherById(1L, "EUR");

        assertNotNull(result);
        assertAll(
                () -> assertEquals("EUR", result.getCurrency()),
                () -> assertEquals(BigDecimal.valueOf(85).setScale(2, RoundingMode.HALF_UP), result.getValue())
        );
    }

    @Test
    void getVoucherByIdWithUnsupportedCurrency() {
        assertThrows(IllegalArgumentException.class, () -> voucherService.getVoucherById(1L, "LEV"),
                "The provided currency is not supported.");
    }

    @Test
    void getAllVouchersWithUnsupportedCurrency() {
        assertThrows(IllegalArgumentException.class, () -> voucherService.getAllVouchers("LEV"),
                "The provided currency is not supported.");
    }

    @Test
    void checkPercentageVoucherLimitValues() {

        voucher.setValue(new BigDecimal("0.00"));
        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher),
                "Percentage voucher value should be between 1 and 100.");

        voucher.setValue(new BigDecimal("101.00"));
        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher),
                "Percentage voucher value should be between 1 and 100.");

        voucher.setValue(new BigDecimal("1.00"));
        assertDoesNotThrow(() -> voucherService.createVoucher(voucher));

        voucher.setValue(new BigDecimal("100.00"));
        assertDoesNotThrow(() -> voucherService.createVoucher(voucher));
    }

    @Test
    void checkAbsoluteTypeVoucherLimitValues() {
        voucher.setDiscountType(DiscountType.AMOUNT);

        voucher.setValue(new BigDecimal("0.00"));
        assertThrows(IllegalArgumentException.class, () -> voucherService.createVoucher(voucher),
                "Fixed value voucher should have a minimum value of 1.");

        voucher.setValue(new BigDecimal("1.00"));
        assertDoesNotThrow(() -> voucherService.createVoucher(voucher));
    }

    private List<Voucher> createVouchersWithCurrency(String currency) {
        List<Voucher> vouchers = new ArrayList<>();
        vouchers.add(createVoucherWithCurrency(1L, currency));
        vouchers.add(createVoucherWithCurrency(2L, currency));
        return vouchers;
    }

    private Voucher createVoucherWithCurrency(long id, String currency) {
        Voucher voucher = new Voucher();
        voucher.setId(id);
        voucher.setValue(BigDecimal.valueOf(100));
        voucher.setCurrency(currency);
        return voucher;
    }
}
