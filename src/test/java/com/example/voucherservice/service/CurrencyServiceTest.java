package com.example.voucherservice.service;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
class CurrencyServiceTest {

    @Autowired
    private CurrencyServiceImpl currencyService;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    void shouldConvertAmountToTargetCurrency() {
        Map<String, Object> responseMap = new HashMap<>();
        responseMap.put("success", true);
        responseMap.put("result", 120.00);

        ResponseEntity<Map<String, Object>> responseEntity = ResponseEntity.ok(responseMap);

        when(restTemplate.exchange(any(String.class), eq(HttpMethod.GET), eq(null), any(ParameterizedTypeReference.class)))
                .thenReturn(responseEntity);

        BigDecimal result = currencyService.convertAmount(new BigDecimal("100.00"), "EUR");
        assertEquals(new BigDecimal("120.00").setScale(2), result);
    }
}
