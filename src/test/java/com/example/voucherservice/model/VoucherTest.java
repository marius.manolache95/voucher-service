package com.example.voucherservice.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class VoucherTest {

    @Test
    void shouldAcceptDiscountType() {
        Voucher voucher = new Voucher();
        voucher.setDiscountType(DiscountType.PERCENTAGE);
        assertEquals(DiscountType.PERCENTAGE, voucher.getDiscountType());
    }
}
