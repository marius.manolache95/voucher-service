package com.example.voucherservice.service;

import com.example.voucherservice.util.Constants;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;

@Service
public class CurrencyServiceImpl implements CurrencyService {

    private final RestTemplate restTemplate;

    public CurrencyServiceImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public BigDecimal convertAmount(BigDecimal value, String targetCurrency) {
        String url = String.format("%s?from=%s&to=%s&amount=%s&apikey=%s", Constants.CURRENCY_EXCHANGE_API_URL, Constants.DEFAULT_CURRENCY, targetCurrency, value, Constants.CURRENCY_EXCHANGE_API_KEY);

        ResponseEntity<Map<String, Object>> responseEntity = restTemplate.exchange(url, HttpMethod.GET, null, new ParameterizedTypeReference<Map<String, Object>>() {});

        Map<String, Object> response = responseEntity.getBody();

        if (response == null || !Boolean.TRUE.equals(response.get("success"))) {
            throw new IllegalStateException("Conversion failed: the response from the external service was null or unsuccessful.");
        }

        BigDecimal convertedAmount = new BigDecimal(response.get("result").toString());

        return convertedAmount.setScale(2, RoundingMode.HALF_UP);
    }
}
