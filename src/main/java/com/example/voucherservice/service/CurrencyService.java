package com.example.voucherservice.service;

import java.math.BigDecimal;

public interface CurrencyService {
    BigDecimal convertAmount(BigDecimal value, String targetCurrency);
}
