package com.example.voucherservice.service;

import com.example.voucherservice.model.DiscountType;
import com.example.voucherservice.model.Voucher;
import com.example.voucherservice.repository.VoucherRepository;
import com.example.voucherservice.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VoucherServiceImpl implements VoucherService {

    private final VoucherRepository voucherRepository;
    private final CurrencyService currencyService;

    @Autowired
    public VoucherServiceImpl(VoucherRepository voucherRepository, CurrencyService currencyService) {
        this.voucherRepository = voucherRepository;
        this.currencyService = currencyService;
    }

    @Override
    public Voucher createVoucher(Voucher voucher) {

        validateVoucherMandatoryFields(voucher);

        validateVoucherValue(voucher);

        validateVoucherIsAlphaNumeric(voucher);

        voucher.setCode(voucher.getCode().toUpperCase());

        Optional<Voucher> existingVoucher = voucherRepository.findByCode(voucher.getCode());

        if (existingVoucher.isPresent()) {
            throw new IllegalArgumentException("Voucher code is already present in the system");
        }

        return voucherRepository.save(voucher);
    }

    public List<Voucher> getAllVouchers(String currency) {

        validateSupportedCurrencies(currency);

        List<Voucher> vouchers = voucherRepository.findAll();

        if (vouchers.isEmpty()) {
            throw new NoSuchElementException("No vouchers found");
        }

        if (!currency.equalsIgnoreCase(Constants.DEFAULT_CURRENCY)) {
            vouchers = vouchers.stream().map(voucher -> {
                BigDecimal convertedValue = currencyService.convertAmount(voucher.getValue(), currency);
                voucher.setValue(convertedValue);
                voucher.setCurrency(currency);
                return voucher;
            }).collect(Collectors.toList());
        }

        return vouchers;
    }

    public Voucher getVoucherById(Long id, String currency) {

        validateSupportedCurrencies(currency);

        Voucher voucher = voucherRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Voucher not found with id: " + id));

        if (!currency.equalsIgnoreCase(Constants.DEFAULT_CURRENCY)) {
            BigDecimal convertedValue = currencyService.convertAmount(voucher.getValue(), currency);
            voucher.setValue(convertedValue);
            voucher.setCurrency(currency);
        }

        return voucher;
    }

    @Override
    public void invalidateVoucher(Long id) {
        Voucher voucher = voucherRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Voucher not found"));
        if (!voucher.getValid()) {
            throw new IllegalStateException("Voucher is already invalidated");
        }
        voucher.setValid(false);
        voucherRepository.save(voucher);
    }

    private void validateSupportedCurrencies(String currency) {

        if (!Constants.supportedCurrencies.contains(currency)) {
            throw new IllegalArgumentException("The provided currency is not supported.");
        }
    }

    private void validateVoucherValue(Voucher voucher) {
        BigDecimal value = voucher.getValue();
        DiscountType discountType = voucher.getDiscountType();

        if (discountType == DiscountType.PERCENTAGE) {
            if (value.compareTo(BigDecimal.ONE) < 0 || value.compareTo(BigDecimal.valueOf(100)) > 0) {
                throw new IllegalArgumentException("Percentage voucher value should be between 1 and 100.");
            }
            return;
        }

        if (discountType == DiscountType.AMOUNT && value.compareTo(BigDecimal.ONE) < 0) {
            throw new IllegalArgumentException("Fixed value voucher should have a minimum value of 1.");
        }
    }

    private void validateVoucherMandatoryFields(Voucher voucher) {
        if (voucher.getCode() == null || voucher.getDiscountType() == null || voucher.getValue() == null ||
                voucher.getValid() == null || voucher.getUsageType() == null) {
            throw new IllegalArgumentException("Incorrect request structure");
        }
    }

    private void validateVoucherIsAlphaNumeric(Voucher voucher) {
        if (!voucher.getCode().matches("^[A-Za-z0-9]+$")) {
            throw new IllegalArgumentException("Invalid voucher code");
        }
    }
}
