package com.example.voucherservice.service;

import com.example.voucherservice.model.Voucher;

import java.util.List;

public interface VoucherService {
    Voucher createVoucher(Voucher voucher);
    List<Voucher> getAllVouchers(String currency);
    Voucher getVoucherById(Long id, String currency);
    void invalidateVoucher(Long id);
}
