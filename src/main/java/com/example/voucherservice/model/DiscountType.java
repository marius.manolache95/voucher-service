package com.example.voucherservice.model;

public enum DiscountType {
    PERCENTAGE,
    AMOUNT
}
