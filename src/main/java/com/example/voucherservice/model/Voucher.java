package com.example.voucherservice.model;

import com.example.voucherservice.util.Constants;
import lombok.Data;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
@NoArgsConstructor
@Entity
@Table(name = "vouchers")
public class Voucher implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false, length = 15)
    private String code;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private DiscountType discountType;

    @Column(name = "\"value\"", nullable = false)
    private BigDecimal value;

    public BigDecimal getValue() {
        if (value == null) {
            return null;
        }
        return value.setScale(2, RoundingMode.HALF_UP);
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    @Transient
    private String currency;

    public String getCurrency() {
        return currency == null ? Constants.DEFAULT_CURRENCY : currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    @Column(nullable = false)
    private Boolean valid;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private UsageType usageType;

    public enum UsageType {
        SINGLE_USE,
        MULTIPLE_USE
    }
}
