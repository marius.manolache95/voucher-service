package com.example.voucherservice.controller;

import com.example.voucherservice.model.Voucher;
import com.example.voucherservice.service.VoucherService;
import com.example.voucherservice.util.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/vouchers")
public class VoucherController {

    private final VoucherService voucherService;

    @Autowired
    public VoucherController(VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @PostMapping
    public ResponseEntity<Voucher> createVoucher(@RequestBody Voucher voucher) {
        Voucher createdVoucher = voucherService.createVoucher(voucher);
        return new ResponseEntity<>(createdVoucher, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<List<Voucher>> getVouchers(@RequestParam(required = false) String currency) {
        List<Voucher> vouchers = voucherService.getAllVouchers(Optional.ofNullable(currency).orElse(Constants.DEFAULT_CURRENCY));
        return ResponseEntity.ok(vouchers);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Voucher> getVoucherById(@PathVariable Long id, @RequestParam(required = false) String currency) {
        Voucher voucher = voucherService.getVoucherById(id, Optional.ofNullable(currency).orElse(Constants.DEFAULT_CURRENCY));
        return ResponseEntity.ok(voucher);
    }

    @PutMapping("/{id}/invalidate")
    public ResponseEntity<Void> invalidateVoucher(@PathVariable Long id) {
        voucherService.invalidateVoucher(id);
        return ResponseEntity.ok().build();
    }

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<String> handleIllegalArgumentException(IllegalArgumentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NoSuchElementException.class)
    public ResponseEntity<String> handleNoSuchElementException(NoSuchElementException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(IllegalStateException.class)
    public ResponseEntity<String> handleIllegalStateException(IllegalStateException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
}
